[< Active Debugging](1-debugging.md)
# Classifying the Defect

### Types of Defects

 - Syntax or Type
 - Typos
 - Implementation
 - Logical


### Syntax or Type

These are always caught by the compiler/interpreter, and reported via error messages. Typically, an error message clearly indicates the cause of error; for instance, the line number, the incorrect piece of code, and an explanation. Such messages usually give enough information about where the problem is and what needs to be done. In addition, IDEs with syntax highlighting can give good indication about such errors even before compiling the program.

### Typos

Typos and other simple errors that have pass undetected by the type-checker or the other checks in the compiler/IDE. Once these are identified, they can easily be fixed. Examples include: spelling errors; calling a non-existent function in an interpreted language (for example, when providing the funciton name for a callback);

### Implementation

Invariant: any rule that must be obeyed throughout the execution of your program that can be communicated to a human, but not to your compiler.

These bugs arise when concrete data structures violate the underlying invariant. For example, a class object property expects an `string` but receives a `bool` or expects an `int` between 1 & 31 (possible days in a month) but is actually 40.

### Logical

Logical bugs are the most difficult to squash. These bugs will compile / execute without crashing but will not produce the expected result. These bugs can be further exascerbated when the algorithm is only flawed for certain corner cases. In many cases it can be helpful to rewrite the algorithm from scratch.

Consider the logically flawed averaging algorithm: `a + b / 2`. This example fails to obey the order of operations.

## References

- [Debugging Techniques](http://www.cs.cornell.edu/courses/cs312/2006fa/lectures/lec26.html)
