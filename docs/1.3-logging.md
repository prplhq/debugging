[< Active Debugging](1-debugging.md)
# Logging

Logging is extremely useful. Any decent application will have sufficient logging in place. Any time an application is misbehaving, logs should be among the first to consult. Not only are they useful for debugging, they are necessary to maintain security in an application. During development errors and traces can be displayed on the screen, but this information can contain sensitive information and should not be visible on ANY publically accessible environment (production and even staging if it is not protected in some way).

There are many ways that logs can be produced. We'll discuss some of those mechanisms now.

## Server Logs

Every running service should produce various logs out-of-the-box. In most *nix systems you will find these logs at `/var/log` and usually in a directory by the same name as the service (ex: `/var/log/nginx`). These logs can be produced from things like syntax errors or unhandled exceptions in your application. Similarly, errors that cause the service itself to crash (segfault) will be logged by the OS.

In our example we've synced the nginx/php logs to `examples/logs/nginx`. In PHP logging levels can be adjusted with `error_reporting($level)`. A `0` will turn off error reporting completely; if you ever come across this, PLEASE fix it. Applications should always log even when those messages are hidden on the front-end (`ini_set('display_errors', 0);`). More info about PHP error reporting can be found in [the documentation](https://www.php.net/manual/en/function.error-reporting.php).


## Application Log Writers

A second way that logs can be produced is by writing these logs from your application. These logs can be written in a variety of severities (debug, info, error, etc). It is wise to log messages throughout your application, particularly in places like try/catch blocks or when other notable events occur.

Log writers should be structured in such a way so as to strike a healthy balance between verbosity that can be set for a given application environment and easily modified as necessary if issues arise in a production environment.

Many frameworks employ their own logging system, and there are many portable logging libraries available. In PHP, one of the most popular libraries is Monolog. It's a very powerful and flexible logger. It can be configured to format logs in many ways and output those logs to many destinations including files, emails, databases and even network streams to be consumed by systems such as logstash.

The default logging strategy for most of these framework loggers is typically to write to files. This is fine for many smaller, single-server applications, however, when an application grows it can become difficult to sift through many lines of log files, even if they are formatted. Configuring a robust strategy can be quite complex. The [ELK/Elastic Stack](https://www.elastic.co/products/) is a popular self-hosted solution for consuming, parsing and displaying logs in a central location.

- [Monolog](https://github.com/Seldaek/monolog/)
- [Zend Log](https://docs.zendframework.com/zend-log/)


### Cloud Loggers

Cloud loggers are a subset of application-level writers. As stated before, centralized logging can be difficult to configure and storing this data can become cumbersome. Enter cloud logging platforms. Platforms like [Bugsnag](https://www.bugsnag.com/) can simplify this task.

