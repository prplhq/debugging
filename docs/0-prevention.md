[< Overview](../readme.md)

# Prevention

This may seem like an odd place to start with talking about debugging, but hunting down bugs can take up a lot of time, therefore, the best situation is to avoid as many bugs as possible in the first place. To do that we can implement a few techniques.

### [Defensive programming](0.0-defensive.md)

### [Typing (Strong / Static Typing)](0.1-types.md)

### [Linting](0.2-linting.md)

### [Testing](0.3-testing.md)
