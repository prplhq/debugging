[< Overview](../readme.md)

# Active Debugging

So we've done everything we reasonably can to prevent bugs from entering our code, but inevitably we're going to wind up with some anyways. What to do?

### [Classifying the Defect](1.0-classify.md)

### [Techniques](1.1-techniques.md)

### [Exceptions](1.2-exceptions.md)

### [Logging](1.3-logging.md)

### [Debuggers](1.4-debuggers.md)
