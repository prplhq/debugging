# Debugging

## Getting Started

- Clone this repo
- From the project root, change into the examples directory: `cd examples`
- Bring up the container: `./up.sh`
- Change into the app directory: `cd app`
- Install yarn deps: `yarn install`
- Install composer deps: `composer install`

![a buuuug](docs/inc/bug.png "It's a BUG!")

Debugging is a very nebulous topic and can differ based on language, platform, implementation, organization etc. That said, in this course we'll try to somewhat categorize the debugging process and look at some methods and tools to debug your code.

**Bug:** A defect in software. An error, flaw, failure or fault in a computer program or system that causes it to produce an incorrect or unexpected result, or to behave in unintended ways.

In this course we'll break down our debugging into 2 primary categories: Prevention and Active Debugging.

### [Prevention](docs/0-prevention.md)

### [Active Debugging](docs/1-debugging.md)
