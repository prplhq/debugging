#!/usr/bin/env bash

BLUE='\033[0;34m'
GREEN='\033[0;32m'
NC='\033[0m'

echo -e "${BLUE}Starting environment${NC}"

docker-compose up -d

echo -e "${BLUE}Checking Hosts file${NC}"
if grep -R "debugging.local" "/etc/hosts"
then
    echo -e "${BLUE}Hosts file is up to date.${NC}"
else
    sudo sh -c 'echo 127.0.0.1 debugging.local >> /etc/hosts'
fi

echo -e ""
echo -e "\033[15;48;5;69m                            All Done. Enjoy!                                     \033[0;49;39m"
echo -e ""
echo -e "${GREEN}http://localhost:3838"
echo -e "${GREEN}http://debugging.local:3838"
echo -e ""

