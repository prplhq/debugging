<?php
declare(strict_types=1);

require __DIR__ . '/../../vendor/autoload.php';


use PHPUnit\Framework\TestCase;
use Model\Car;
use ModelInterface\IVehicle;

final class VehicleTest extends TestCase
{
    public function testVehicleType(): void
    {
      $car = new Car();
      $this->assertInstanceOf(IVehicle::class, $car);
      $this->assertInstanceOf(Car::class, $car);
    }

    public function testAccelerate(): void
    {
      $car = new Car();
      $this->assertEquals(0, $car->getSpeed());
      $car->accelerate(5);
      $this->assertEquals(5, $car->getSpeed());
    }

}
