<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>Test This</title>

  <link rel="stylesheet" href="/assets/bootstrap.min.css">
</head>
<body>


  <header class="bg-primary text-white p-3">
    <div class="container text-center">
      <h1>Test This!</h1>
    </div>
  </header>

  <section id="content" class="mt-5 mb-5">
    <div class="container">

      <div class="row mb-5">
        <div class="col-lg-6 mx-auto">
          <div class="form-group">
            <label for="inputA">Value</label>
            <input type="text" class="form-control" id="inputA" aria-describedby="inputAHelp" value="4" placeholder="Value A">
            <small id="inputAHelp" class="form-text text-muted">Enter a number</small>
          </div>
        </div>
      </div>

      <div class="row mb-5">
        <div class="col-lg-3">
          <button id="add" class="btn btn-primary btn-block">Add</button>
        </div>
        <div class="col-lg-3">
          <button id="multiply" class="btn btn-primary btn-block">Multiply</button>
        </div>
        <div class="col-lg-3">
          <button id="divide" class="btn btn-primary btn-block">Divide</button>
        </div>
        <div class="col-lg-3">
          <button id="mean" class="btn btn-primary btn-block">Mean</button>
        </div>
      </div>

      <div class="row mb-5">
        <div class="col-lg-3">
          <button id="clear" class="btn btn-outline-danger btn-block">Clear</button>
        </div>

        <div class="col-lg-3">
          <h4>Results:</h4>
          <div id="results"></div>
        </div>
        <div class="col-lg-3">
          <h4>Mean:</h4>
          <div id="results-mean"></div>
        </div>
      </div>

    </div>
  </section>

  <script src="dist/index.bundle.js"></script>

</body>
</html>