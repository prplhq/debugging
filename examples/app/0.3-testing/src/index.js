import { add, divide, mean, multiply } from './inc/math-utils';
import { camelCase, capitalize } from './inc/string-utils';

// set up vars
let results = [0];
let results_mean = null;
let resultsEl = document.getElementById("results");
let resultsMeanEl = document.getElementById("results-mean");
let inputA = document.getElementById("inputA");

// update the dom
function update() {
  let html = results.map(item => {
    return `<li>${item}</li>`;
  });
  resultsEl.innerHTML = "<ul>" + html.join("") + "</ul>";
  resultsMeanEl.innerHTML = results_mean ? `<strong>${results_mean}</strong>` : "";
}

// add listener
document.getElementById("add").addEventListener('click', function(e){
  e.preventDefault();
  if (inputA.value) {
    results.push(add(parseInt(inputA.value, 10), parseInt(results[results.length-1], 10)));
    update();
  }
});

// multiply listener
document.getElementById("multiply").addEventListener('click', function(e){
  e.preventDefault();
  if (inputA.value) {
    results.push(multiply(parseInt(inputA.value, 10), parseInt(results[results.length-1], 10)));
    update();
  }
});

// divide listener
document.getElementById("divide").addEventListener('click', function(e){
  e.preventDefault();
  if (inputA.value) {
    results.push(divide(parseInt(results[results.length-1], 10), parseInt(inputA.value, 10)));
    update();
  }
});

// mean listener
document.getElementById("mean").addEventListener('click', function(e){
  e.preventDefault();
  if (results.length) {
    results_mean = mean(results);
    update();
  }
});

// clear listener
document.getElementById("clear").addEventListener('click', function(e){
  e.preventDefault();
  results = [0];
  results_mean = null;
  update();
});

// trigger an update
update();
