// ./test/index.spec.js
import { expect } from "chai"
import { add, divide, mean, multiply } from "./math-utils";

describe("Math Functions", () => {

  describe("add function", () => {
    it("should add 2 and 4!", () => {
      const sum = add(2, 4);
      expect(sum).to.equal(6)
    })
  })

  describe("divide function", () => {
    it("should divide 2 from 4!", () => {
      const div = divide(4, 2);
      expect(div).to.equal(2)
    })
  })

  describe("divide function", () => {
    it("should be Infinity when dividing by 0!", () => {
      const div = divide(4, 0);
      expect(div).to.equal(Infinity)
    })
  })

  describe("mean function", () => {
    it("should find the mean of 2 and 4!", () => {
      const average = mean([2, 4]);
      expect(average).to.equal(3)
    })
  })

  describe("multiple function", () => {
    it("should multiple 2 and 4!", () => {
      const mult = multiply(2, 4);
      expect(mult).to.equal(8)
    })
  })


})