describe('Math', function() {

  beforeEach(function() {
    cy.visit('/0.3-testing/index.php')
  })

  it('Testing Math UI', function() {

    cy.get('#inputA').as('inputA')
    cy.get('#results').as('results')
    cy.get('#results-mean').as('resultsMean')

    cy.get('header h1').should('contain', 'Test This!')

    cy.get("@inputA").clear().type(4)
    cy.get('#add').click()
    cy.get('@results').should('contain', '4')

    cy.get('@inputA').clear().type(5)
    cy.get('#multiply').click()
    cy.get('@results').should('contain', '20')

    cy.get('@inputA').clear().type(10)
    cy.get('#divide').click()
    cy.get('@results').should('contain', '2')

    cy.get('@inputA').clear().type(22)
    cy.get('#add').click()
    cy.get('#mean').click()
    cy.get('@resultsMean').should('contain', '10')

    cy.get('#clear').click()
    cy.get('@results').should('not.contain', '4')
    cy.get('@resultsMean').should('not.contain', '10')

  })
})
