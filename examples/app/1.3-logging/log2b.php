<h1>Starting Zend Log Script</h1>

<h3>Zend Log: <a href="https://docs.zendframework.com/zend-log/">Documentation</a></h3>
<?php

// composer autoloader
require __DIR__ . '/../vendor/autoload.php';

use Zend\Log\Logger;
use Zend\Log\Writer\Stream;
use Zend\Log\Filter\Priority;

$logger = new Logger; // set up logger
$writer = new Stream('zend.log'); // set up writer
// $filter = new Priority(Logger::ERR); // filter below ERR
$filter = new Priority(Logger::DEBUG); // filter below WARN
$writer->addFilter($filter); // add filter to writer
$logger->addWriter($writer); // add writer to logger

// log a series of messages
$logger->debug('Debug message'); // level: 7
$logger->info('Informational message'); // level: 6
$logger->notice('Notice message'); // level: 5
$logger->warn('Warning message'); // level: 4
$logger->err('Error message'); // level: 3
$logger->crit('Critical message'); // level: 2
$logger->alert('Alert message'); // level: 1
$logger->emerg('Emergency message'); // level: 0

echo "<h2>done logging</h2>";
