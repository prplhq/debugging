<h1>Starting Monolog Script</h1>

<h3>Monolog Log: <a href="https://github.com/Seldaek/monolog/">Documentation</a></h3>
<?php

// composer autoloader
require __DIR__ . '/../vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\LineFormatter;

// Create some loggers
$logger = new Logger('Monolog');
$securityLogger = new Logger('Security');

// Create a handler
$fileStream = new StreamHandler(__DIR__.'/monolog.log', Logger::DEBUG);

// Create a formatter
$dateFormat = "c";
$output = "[%datetime%] %channel%: %level_name% (%level%): %message% %context% %extra%\n";
$formatter = new LineFormatter($output, $dateFormat, false, true);
$fileStream->setFormatter($formatter);

// add the handler to the loggers
$logger->pushHandler($fileStream);
$securityLogger->pushHandler($fileStream);

// log a series of messages
$logger->debug('Debug message'); // level: 100
$logger->info('Informational message'); // level: 200
$logger->notice('Notice message'); // level: 250
$logger->warn('Warning message'); // level: 300
$logger->err('Error message'); // level: 400
$logger->crit('Critical message'); // level: 500

$securityLogger->alert('Alert message'); // level: 550
$securityLogger->err('Error Message', ['some' => 'data']); // level: 600

echo "<h2>done logging</h2>";
