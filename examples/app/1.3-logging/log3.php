<h1>Starting Bugsnag Script</h1>

<h3>Bugsnag: <a href="https://docs.bugsnag.com/platforms/php/other/">Documentation</a></h3>
<?php

// composer autoloader
require __DIR__ . '/../vendor/autoload.php';
// TestingTesting123

use Bugsnag\Client;
use Bugsnag\Handler;

$bugsnag = Client::make('698be526e6b4bcbbb5b1a50c0010b1f0');
// register bugsnag as the error/exception handler:
// set_error_handler() and set_exception_handler()
Handler::register($bugsnag);


ini_set('display_errors', 0);

// try to access non-existent variable
echo $nonExistent;

// try to foreach on non-iterable
$number = 1;
foreach ($number as $num) {
  echo $num;
}

// try to access non-existent index
$arr = [];
echo $arr['missing index'];

// try to call non-existent function
nonExistent();

echo "<h2>done logging</h2>";
