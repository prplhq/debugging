<h1>Starting Server Logging Script</h1>

<?php

// ini_set('display_errors', 0);

// try to access non-existent variable
echo $nonExistent;

// try to foreach on non-iterable
$number = 1;
foreach ($number as $num) {
  echo $num;
}

// try to access non-existent index
$arr = [];
echo $arr['missing index'];

// try to call non-existent function
nonExistent();

echo "<h2>done logging</h2>";
