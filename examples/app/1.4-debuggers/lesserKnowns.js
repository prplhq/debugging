const flatArr = ['this', 'is', 'an', 'array'];

const notSoFlatArr = [
    ["John", "Smith", 42, { title: "Mr." }],
    ["Jane", "Doe", 12, { title: "Mrs." }],
    ["Emily", "Jones", 76, { title: "Ms." }]
];

const flatObj = {
    name: 'Jackie',
    age: 50,
};

const notSoFlatObj = [
    {
        name: 'Mike',
        age: 32,
    },
    {
        name: 'Jack',
        age: 10,
    },
    {
        name: 'Roger Murtagh',
        age: 'Too Old For This Shit',
    }
]

function superUseful(amount) {
    let items = [];

    for (let i=0; i < amount; i++) {
        items.push({ index: i });
    }
}

// Display
console.table(flatArr);
console.table(notSoFlatArr);
console.table(flatObj);
console.table(notSoFlatObj);

// Benchmarking
console.time('useful function timer');
superUseful(1000000);
console.timeEnd('useful function timer');

// Stack traces
function foo() {
    function bar() {
        console.trace();
    }
    bar();
}

foo();

// fancy, over-the-top, unrelated, console styles

const styles = [
    'background: linear-gradient(#D33106, #571402)',
    'border: 1px solid #3E0E02',
    'color: white',
    'display: block',
    'text-shadow: 0 1px 0 rgba(0, 0, 0, 0.3)',
    'box-shadow: 0 1px 0 rgba(255, 255, 255, 0.4) inset, 0 5px 3px -5px rgba(0, 0, 0, 0.5), 0 -13px 5px -10px rgba(255, 255, 255, 0.4) inset',
    'line-height: 40px',
    'text-align: center',
    'font-weight: bold',
    'margin: 15px',
    'padding: 20px 100px'
].join(';');

console.log('%c spicy', styles);

// Additional reference
// https://developer.mozilla.org/en-US/docs/Web/API/console