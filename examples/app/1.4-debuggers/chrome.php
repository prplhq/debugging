<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="chrome.css"/>
  <title>Autocomplete</title>
</head>
<body>
  <div id="autocomplete-container">
    <input type="text" autofocus="true" name="autofocus sample" placeholder="search people" id="autocomplete-input" />
    <ul id="autocomplete-results"></ul>
  </div>
  <script src="chrome.js"></script>
</body>
</html>