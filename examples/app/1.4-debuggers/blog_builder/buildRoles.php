<?php
// Create Roles
use Workflow\RoleFactory;
use Workflow\PermissionHelper;
use Workflow\RoleList;

function initializeRoles() {
    $roleList = RoleList::get_instance();
    $permissionList = PermissionHelper::get_instance();
    $roleFactory = new RoleFactory();

    $publish = $permissionList->get_permission('publish');
    $unpublish = $permissionList->get_permission('unpublish');
    $approve = $permissionList->get_permission('approve');
    $reject = $permissionList->get_permission('reject');
    $requestReview = $permissionList->get_permission('requestReview');
    $edit = $permissionList->get_permission('edit');
    $editOwn = $permissionList->get_permission('editOwn');
    $create = $permissionList->get_permission('create');
    $delete = $permissionList->get_permission('delete');

    $admin = $roleFactory->buildRole('admin', [
        $publish,
        $unpublish,
        $approve,
        $reject,
        $requestReview,
        $edit,
        $editOwn,
        $create,
        $delete
    ]);
    $roleList->add_permission($admin);

    $publisher = $roleFactory->buildRole('publisher', [
        $publish,
        $unpublish,
    ]);
    $roleList->add_permission($publisher);

    $editor = $roleFactory->buildRole('editor', [
        $approve,
        $reject,
        $edit,
    ]);
    $roleList->add_permission($editor);

    $author = $roleFactory->buildRole('author', [
        $requestReview,
        $editOwn,
        $create,
        $delete
    ]);
    $roleList->add_permission($author);
}

