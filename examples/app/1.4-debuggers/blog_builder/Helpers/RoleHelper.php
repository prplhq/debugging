<?php

namespace Workflow\Helpers;

use Workflow\Publishing\Role;

class RoleHelper {

    private static $instance = NULL;
    public $roles = [];

    private function __construct() {
    }

    public function add_role($role) {
        $this->roles[$role->name] = $role;
    }

    /**
     * @param $roleName
     * @return bool|Role
     */
    public function get_role($roleName) {
        return $this->roles[$roleName] ?? FALSE;
    }

    public static function get_instance() {
        if (self::$instance == null) {
            self::$instance = new RoleHelper();
        }

        return self::$instance;
    }
}
