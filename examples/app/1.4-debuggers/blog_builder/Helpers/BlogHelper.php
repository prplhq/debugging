<?php

namespace Workflow\Helpers;

use Workflow\Publishing\Blog;
use Workflow\Publishing\User;

class BlogHelper {

    private static $instance;
    private $blog_id_index;
    public $blogs = [];

    private function __construct() {
        $this->blog_id_index = 0;
    }

    public function addBlog($blog) {
        $this->blogs[$blog->id] = $blog;
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function &getBlog($id) {
        return $this->blogs[$id] ?? FALSE;
    }

    public function &getAllBlogs() {
        return array_map(function ($blog_id) {
            return $this->blogs[$blog_id];
        }, $this->blogs);
    }

    public static function getInstance() {
        if (self::$instance == null) {
            self::$instance = new BlogHelper();
        }

        return self::$instance;
    }

    /**
     * @param User $user
     * @param $title
     * @param $options
     * @return bool|Blog
     * @throws \Exception
     */
    public function createBlog($user, $title, $options) {
        if (!$user->hasPermission('create')) {
            return false;
        }

        return new Blog($this->blog_id_index, $title, $user, $options);
    }
}
