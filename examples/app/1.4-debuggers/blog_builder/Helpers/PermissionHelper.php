<?php

namespace Workflow\Helpers;

class PermissionHelper {

    private static $instance = NULL;
    public $permissions = [];

    private function __construct() {
    }

    public function add_permission($permission) {
        $this->permissions[$permission->name] = $permission;
    }

    /**
     * @param $permission_name
     * @return bool|Role
     */
    public function &get_permission($permission_name) {
        return $this->permissions[$permission_name] ?? FALSE;
    }

    public function &get_all_permissions() {
        return array_map(function ($permission_name) {
            return $this->permissions[$permission_name];
        }, $this->permissions);
    }

    public static function get_instance() {
        if (self::$instance == null) {
            self::$instance = new PermissionHelper();
        }

        return self::$instance;
    }
}
