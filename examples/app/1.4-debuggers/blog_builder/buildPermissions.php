<?php
// Create Roles
use Workflow\Permission;
use Workflow\Helper\PermissionHelper;

function initializeRoles() {
    $permissionHelper = PermissionHelper::get_instance();

    $canPublish = new Permission('publish');
    $permissionHelper->add_permission($canPublish);

    $canUnpublish = new Permission('unpublish');
    $permissionHelper->add_permission($canUnpublish);

    $canApprove = new Permission('approve');
    $permissionHelper->add_permission($canApprove);

    $canReject = new Permission('reject');
    $permissionHelper->add_permission($canReject);

    $canRequestReview = new Permission('request_review');
    $permissionHelper->add_permission($canRequestReview);

    $canEdit = new Permission('edit');
    $permissionHelper->add_permission($canEdit);

    $canEditOwn = new Permission('edit_own');
    $permissionHelper->add_permission($canEditOwn);

    $canCreate = new Permission('create');
    $permissionHelper->add_permission($canCreate);

    $canDelete = new Permission('delete');
    $permissionHelper->add_permission($canDelete);
}

