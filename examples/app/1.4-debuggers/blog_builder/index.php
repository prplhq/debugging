<?php

// composer autoloader
require __DIR__ . '/../../vendor/autoload.php';

use Workflow\Helpers\BlogHelper;
use Workflow\Helpers\RoleHelper;
use Workflow\Publishing\UserFactory;

$userFactory = new UserFactory();
$roleList = RoleHelper::get_instance();

// Get roles
$admin = $roleList->get_role('admin');
$publisher = $roleList->get_role('publisher');
$editor = $roleList->get_role('editor');
$author = $roleList->get_role('author');

// Build users
$alice = $userFactory->buildUser('alice', [$admin]);
$bob = $userFactory->buildUser('bob', [$publisher]);
$charlie = $userFactory->buildUser('charlie', [$editor, $author]);
$dennis = $userFactory->buildUser("dennis", [$author]);

// Do things
$blogHelper = BlogHelper::getInstance();

try {
    $blogHelper->createBlog($dennis, 'Hello, World!', ['body' => 'this is the body']);
    $blogHelper->createBlog($charlie, 'Charlie\'s world', ['body' => 'la la la la']);
} catch (\Exception $exception) {
    echo $exception;
}

