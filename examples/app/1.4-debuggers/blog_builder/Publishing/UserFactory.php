<?php

namespace Workflow\Publishing;

class UserFactory {
    protected $idIndex;

    /**
     * UserFactory constructor.
     */
    public function __construct() {
        $this->idIndex = 0;
    }

    public function buildUser($username, $roles) {
        return new User($this->idIndex++, $username, $roles);
    }
}
