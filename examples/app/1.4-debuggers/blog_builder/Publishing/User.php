<?php

namespace Workflow\Publishing;

class User {
    private $id;
    private $roles;
    private $username;

    /**
     * User constructor.
     * @param $id
     * @param $username
     * @param $roles
     */
    public function __construct($id, $username, $roles) {
        $this->id = $id;
        $this->username = $username;
        $this->roles = $roles;
    }

    /**
     * @return mixed
     */
    public function getUsername() {
        return $this->username;
    }

    /**
     * @return
     */
    public function getRoles() {
        return $this->roles;
    }

    /**
     * @param $roles
     */
    public function setRoles($roles): void {
        $this->roles = $roles;
    }

    /**
     * @param $role
     * @return bool
     */
    public function hasRole($role) {
        return !empty($this->roles[$role->id]);
    }

    /**
     * @param $role
     */
    public function addRole($role) {
        $this->roles[$role->id] = $role;
    }

    /**
     * @param $permission_name
     * @return bool
     */
    public function hasPermission($permission_name) {
        foreach ($this->roles as $role) {
            if ($role->hasPermission($permission_name)) {
                return true;
            }
        }

        return false;
    }
}