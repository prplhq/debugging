<?php

namespace Workflow\Publishing;

class Permission {
    private $name;

    /**
     * Permission constructor.
     * @param $name
     */
    public function __construct($name) {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    public function __toString() {
        return $this->name;
    }
}