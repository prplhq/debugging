<?php

namespace Workflow\Publishing;

use DateTime;

class Blog {
    private $id;
    private $title;
    private $author;
    private $created;
    private $body;
    private $state;

    /**
     * Blog constructor.
     * @param $id
     * @param $title
     * @param $author
     * @param $options
     * @throws \Exception
     */
    public function __construct($id, $title, $author, $options) {
        $this->id = $id;
        $this->title = $title;
        $this->author = $author;
        $this->body = $options['body'] ?? '';
        $this->created = $options['created'] ?? new DateTime();
        $this->state = $options['state'];
    }

    /**
     * @return string
     */
    public function getBody(): string {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody(string $body): void {
        $this->body = $body;
    }

    /**
     * @return mixed
     */
    public function getState() {
        return $this->state;
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getAuthor() {
        return $this->author;
    }

    /**
     * @return DateTime
     */
    public function getCreated(): DateTime {
        return $this->created;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void {
        $this->title = $title;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author): void {
        $this->author = $author;
    }

    /**
     * @param DateTime $created
     */
    public function setCreated(DateTime $created): void {
        $this->created = $created;
    }
}