<?php

namespace Workflow\Publishing;

class Role {
    private $id;
    private $name;
    private $permissions;

    /**
     * Role constructor.
     * @param $id
     * @param $name
     * @param $permissions
     */
    public function __construct($id, $name, $permissions) {
        $this->id = $id;
        $this->name = $name;
        $this->permissions = $permissions;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPermissions() {
        return $this->permissions;
    }

    /**
     * @param $permissions
     */
    public function setPermissions($permissions): void {
        $this->permissions = $permissions;
    }

    /**
     * @param $permission
     */
    public function addPermission($permission): void {
        $this->permissions[$permission->id] = $permission;
    }

    /**
     * @param $permission
     */
    public function removePermission($permission): void {
        unset($this->permissions[$permission->id]);
    }

    /**
     * @param $permission_name
     * @return bool
     */
    public function hasPermission($permission_name): bool {
        return !is_null($this->permissions[$permission_name]);
    }
}