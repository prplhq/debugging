<?php

namespace Workflow\Publishing;

class RoleFactory {
    protected $idIndex;

    /**
     * UserFactory constructor.
     */
    public function __construct() {
        $this->idIndex = 0;
    }

    public function buildRole($name, $permissions) {
        return new Role($this->idIndex++, $name, $permissions);
    }
}
