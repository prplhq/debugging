<?php

namespace Workflow\Publishing;

class BlogState {
    private $id;
    private $name;
    private $requiredPermissions;

    /**
     * BlogState constructor.
     * @param $id
     * @param $name
     * @param $requiredPermissions
     */
    public function __construct($id, $name, $requiredPermissions) {
        $this->id = $id;
        $this->name = $name;
        $this->requiredPermissions = $requiredPermissions;
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getRequiredPermissions() {
        return $this->requiredPermissions;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }
}
