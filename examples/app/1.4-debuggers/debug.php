<h1>Starting Debug Script</h1>

<?php

$a = 10;
$b = 5;

$mult = mult($a, $b);
echo "<h2>Multiply:</h2>";
echo "$a x $b = $mult";

$div = div($a, $b);
echo "<h2>Divide:</h2>";
echo "$a ÷ $b = $div";


// multiply
function mult($x1, $x2)
{
  return $x1 * $x2;
}

// divide
function div($x1, $x2)
{
  return $x1 / $x2;
}
