<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Squash dem buugs #learnt</title>

  <link rel="stylesheet" href="/assets/bootstrap.min.css">

</head>

<body id="page-top">


  <header class="bg-primary text-white p-3">
    <div class="container text-center">
      <h1>Let's debug!</h1>
    </div>
  </header>

  <section id="content" class="mt-5 mb-5">
    <div class="container">


      <!-- Defensive Programming -->
      <div class="row">
        <div class="col-lg-10 mx-auto">
          <h4>Defensive Programming</h4>
          <ul>
            <li>Go to the gym and <a href="/0.0-defensive/exercise.php?name=%3Cscript%3Ealert%28%27hi%27%29%3C%2Fscript%3E">exercise</a></li>
          </ul>
        </div>
      </div>


      <!-- Types and Static Analysis -->
      <div class="row">
        <div class="col-lg-10 mx-auto">
          <h4>Types and Static Analysis</h4>
          <ul>
            <li>This might have <a href="/0.1-types/static.php">issues</a></li>
          </ul>
        </div>
      </div>


      <!-- Testing -->
      <div class="row">
        <div class="col-lg-10 mx-auto">
          <h4>Test this</h4>
          <ul>
            <li><a href="/0.3-testing/index.php">Test</a> all the things</li>
          </ul>
        </div>
      </div>


      <!-- Debugging techniques -->
      <div class="row">
        <div class="col-lg-10 mx-auto">
          <h4>Shit'z broke</h4>
          <ul>
            <li><a href="/1.x-debugging/stacktrace/index.php">Stack trace</a> is stacked</li>
            <li>Get ready for <a href="/1.x-debugging/isprime/index.php">prime time</a></li>
          </ul>
        </div>
      </div>


      <!-- Exceptions -->
      <div class="row">
        <div class="col-lg-10 mx-auto">
          <h4>Throwing Exceptions</h4>
          <ul>
            <li><a href="/1.2-exceptions/exception1.php">Throw an Exception</a></li>
            <li><a href="/1.2-exceptions/exception2.php">Try/Multiple-Catches</a></li>
            <li><a href="/1.2-exceptions/exception3.php">Try/Catch/Finally</a></li>
          </ul>
        </div>
      </div>


      <!-- Logging -->
      <div class="row">
        <div class="col-lg-10 mx-auto">
          <h4>Let's Log some stuff</h4>
          <ul>
            <li>Server <a href="/1.3-logging/log1.php">logging</a></li>
            <li>Log with <a href="/1.3-logging/log2.php">monolog</a> or <a href="/1.3-logging/log2b.php">zend log</a></li>
            <li>Log <a href="/1.3-logging/log3.php">bugsnag</a></li>
          </ul>
        </div>
      </div>


      <!-- Debuggers -->
      <div class="row">
        <div class="col-lg-10 mx-auto">
          <h4>Debuggers</h4>
          <ul>
            <li>Learn some of the <a href="/1.4-debuggers/lesserKnowns.html">lesser known</a> console logging</li>
            <li>Try out the <a href="/1.4-debuggers/chrome.php">Chrome Debugger</a></li>
            <li>Try out <a href="/1.4-debuggers/debug.php">Xdebug</a></li>
          </ul>
        </div>
      </div>


      <div class="mb-5">&nbsp;</div>

    </div>
  </section>

  <!-- Footer -->
  <footer class="py-4 bg-dark" style="position: fixed; width: 100%; bottom: 0;">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; PSSHHHH 2019</p>
    </div>
    <!-- /.container -->
  </footer>

</body>

</html>