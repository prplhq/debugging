import path from "path";

export default {
  entry: {
    index: ["./0.3-testing/src/index.js"],
  },
  output: {
    path: path.resolve(__dirname, "0.3-testing", "dist"),
    filename: "[name].bundle.js"
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: ['babel-loader'],
        exclude: /node_modules/
      }
    ]
  }
};