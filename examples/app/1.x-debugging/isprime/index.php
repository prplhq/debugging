<?php include_once './utilities.php' ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Prime Time</title>
    <link rel="stylesheet" href="/assets/bootstrap.min.css">

  </head>

  <body>
    <header class="bg-primary text-white p-3">
      <div class="container text-center">
        <h1>Prime Time</h1>
      </div>
    </header>

    <section id="content" class="mt-5 mb-5">
      <div class="container">

        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h2>There are 3 bugs</h2>
            <p><small><em>Find 'em, classify 'em, squash 'em</em></small></p>

            <h4>Tests</h4>
            <p>is 5 a prime? <?= isPrime(5) ? 'Yes' : 'No' ?></p>
            <p>is 11 a prime? <?= isPrime(11) ? 'Yes' : 'No' ?></p>
            <p>is 17 a prime? <?= isPrime(17) ? 'Yes' : 'No' ?></p>
            <p>is 22 a prime? <?= isPrime(22) ? 'Yes' : 'No' ?></p>
            <p>is 997 a prime? <?= isPrime(997) ? 'Yes' : 'No' ?></p>
            <p>is 3531 a prime? <?= isPrime(3531) ? 'Yes' : 'No' ?></p>

            <h4>Lets count Primes</h4>
            <p>How many primes between 4 – 12? <?= countPrimesInRange(10, 12) ?></p>

          </div>
        </div>
      </div>
    </section>


  </body>
</html>
