<?php

/**
 * Returns true if the divisor factors evenly into the dividend. Returns false if it does not
 *
 * @param $divisor
 * @param $dividend
 * @return bool
 */
function isFactor($divisor, $dividend) {
    return $divisor % $dividend === 0;
}

/**
 * Finds all factors of the provided number
 *
 * @param $number
 * @return array - list of all numbers that are factors of the provided number
 */
function findFactors($number) {

    // Get every number between 0 and the number we are checking
    $candidates = range(0, $number);

    // Create an array to hold our factors as we find them
    $foundFactors = [];

    // Iterate through all the numbers smaller than the original number
    foreach ($candidates as $candidate) {
        // Add the number to our list if it is a Factor
        if (isFactor($candidate, $number)) {
            $foundFactors[]= $candidate;
        }
    }

    return $foundFactors;
}

/**
 * Returns true if the provided number is a prime number, false if it is not
 *
 * @param $number
 * @return bool
 */
function isPrime($number) {
    return count(findFactors($number)) === 2;
}

/**
 * Counts the number of prime numbers between the provided numbers (Inclusive)
 *
 * @param $min
 * @param $max
 * @return int
 */
function countPrimesInRange($min, $max) {

    // Get every number between the min and the max
    $candidates = range($min, $max);

    // Create an array to hold our factors as we find them
    $primeCount = 0;

    // Iterate through all the numbers
    foreach ($candidates as $candidate) {
        // Add the number to our list if it is a Factor
        if (isPrime($candidate)) {
            $primeCount++;
        }
    }

    return $primeCount;
}
