<?php
header('X-XSS-Protection:0');

// undefined variable
echo $pi;


// undefined function
echo sum(2,3);


// No such file
$file = file_get_contents(__DIR__ . "/poem.txt");
echo $file;


// undefined offset
$bookIveRead = [
  1 => "Moby Dick",
  3 => "The Great Gatsby",
  42 => "Hitchhiker's Guide to the Galaxy",
];
echo $bookIveRead[10];


// division by zero
$a = 1;
$b = 0;
$div = $a / $b;


// PDO extension not installed
$conn = new PDO('mysql:host=invalidhost;dbname=invaliddb;charset=utf8mb4', 'username', 'password');
$stmt = $conn->prepare('SELECT * FROM users WHERE AND active=:active');
$stmt->execute(['active' => 1]);
$user = $stmt->fetch();


// trusting user input
echo $_GET['name'];


// too few arguments
printDateInterval('2019-01-01 00:00:00');
function printDateInterval($start, $end=null) {
  $startDate = DateTime::createFromFormat('Y-m-d H:i:s', $start);
  $endDate = DateTime::createFromFormat('Y-m-d H:i:s', $end);
  $interval = $startDate->diff($endDate);
  echo $interval->days;
}

