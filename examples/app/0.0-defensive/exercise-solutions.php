<?php
header('X-XSS-Protection:0');

// undefined variable
echo !empty($pi) ? $pi : "3.14";
$pi = !empty($pi) ? $pi : "3.14";
echo $pi ?? "3.14"; // null coalesce
echo PHP_EOL . "<br>";



// undefined function
echo function_exists('sum') ? sum(2,3) : "define 'sum' ya idiot";
echo PHP_EOL . "<br>";



// No such file
$path = __DIR__ . "/poem.txt";
if (is_readable($path)) {
  $file = file_get_contents($path);
  echo $file;
} else {
  echo "could not find file";
}
echo PHP_EOL . "<br>";

$contents = is_readable($path) ? file_get_contents($path) : "no file";
echo $contents;



// undefined offset
$defaults = [
  "favorite" => "Programming for dummies"
];

$bookIveRead = [
  "one" => "Moby Dick",
  "three" => "The Great Gatsby",
  "fortytwo" => "Hitchhiker's Guide to the Galaxy",
];
echo array_merge($defaults, $bookIveRead)["favorite"];
echo PHP_EOL . "<br>";



try {
  // division by zero
  $a = 1;
  $b = 0;
  if ($b == 0) {
    throw new Exception("suuupid", 1);

  }
  $div = $a / $b;
  $div = $b != 0 ? $a / $b : 0;
  // echo $div;
  // echo PHP_EOL . "<br>";
} catch (\Throwable $th) {
  echo "nooope";
}



// PDO extension not installed
try {
  $conn = new PDO('mysql:host=invalidhost;dbname=invaliddb;charset=utf8mb4', 'username', 'password');
  $stmt = $conn->prepare('SELECT * FROM users WHERE AND active=:active');
  $stmt->execute(['active' => 1]);
  $user = $stmt->fetch();
} catch (\Throwable $th) {
  echo $th->getMessage();
  echo PHP_EOL . "<br>";
}



// trusting user input
echo htmlentities($_GET['name']);
echo strip_tags($_GET['name']);
echo filter_var($_GET['name'], FILTER_SANITIZE_STRING);
echo PHP_EOL . "<br>";



// too few arguments
printDateInterval('2019-01-01 00:00:00');
function printDateInterval($start, $end=null) {
  $startDate = DateTime::createFromFormat('Y-m-d H:i:s', $start);
  $endDate = $end ? DateTime::createFromFormat('Y-m-d H:i:s', $end) : new DateTime();
  $interval = $startDate->diff($endDate);
  echo $interval->days;
  echo PHP_EOL . "<br>";
}

