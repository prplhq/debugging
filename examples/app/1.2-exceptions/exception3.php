<h1>Try/Catch/Finally Script</h1>

<?php

try {
  echo "doing a thing<br>";
  throw new Exception("Your stupid");
}

catch (\Exception $e) {
  echo "<pre>";
  echo("Catch Exception: " . $e->getMessage(). PHP_EOL);
}

finally {
  echo "This always runs";
}
