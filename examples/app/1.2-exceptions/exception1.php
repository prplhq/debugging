<h1>Throw an Exception Script</h1>

<?php

try {
  echo "doing a thing<br>";
  throw new Exception("Your stupid");
}

catch (\Exception $e) {
  echo "<pre>";
  echo("Catch Exception: " . $e->getMessage(). PHP_EOL);
}
