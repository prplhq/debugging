<h1>Try/Multiple-Catches Script</h1>

<?php
// composer autoloader
require __DIR__ . '/../vendor/autoload.php';

use Exceptions\ExceptionOne;
use Exceptions\ExceptionTwo;

try {
  echo "doing a thing<br>";
  throw new ExceptionOne("Your stupid");
  throw new ExceptionTwo("Your stupid");
}

catch (ExceptionOne $e) {
  echo "<pre>";
  echo("Exception One: " . $e->getMessage(). PHP_EOL);
}

catch (ExceptionTwo $e) {
  echo "<pre>";
  echo("Exception Two: " . $e->getMessage() . PHP_EOL);
}
