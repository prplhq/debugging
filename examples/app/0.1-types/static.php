<?php

// my custom autoloader
$autoload = function ($class) {
include __DIR__ . "/" . str_replace('\\', '/', $class) . '.php';
};
spl_autoload_register($autoload);

use Model\Car;
use Model\Garage;

$car = new Car();
$car->accelerate(); // method from class
$car->brake(); // method from parent class
$car->getTemp(); // method from trait

// create a new garage
$garage1 = new Garage('My Garage');
// add it to out car's list of garages
$car->addGarage($garage1);

// list our car's garages
$garages = $car->getGarages();
foreach ($garages as $garage) {
echo $garage . "\n";
}

Garage::fixVehicle($car);
