<?php

Namespace ModelInterface;

Interface IVehicle {
    public function brake();
    public function accelerate();
}
