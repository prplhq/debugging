// TYPESCRIPT
// npm install -g typescript
// tsc types.ts


// some static analysis
function rand(){
  return Math.floor(Math.random() * Math.floor(2));
}

if (rand == 1) { // comparing the function not the return value
  let name: string = "Chris"; // variable in this scope never used
}
let welcome: string = "Hi " + name;


// --------------------------------------
// casting
let question: string = "The meaning of life, the universe and everything: ";
let num: number = 42;
let deepThoughtSays: string = question + num as string;
console.log(deepThoughtSays);


// --------------------------------------
// set up a function
let a: string = "5";
let b: number = 10;
function mult(a:number, b:number) {
  return a * b;
}
console.log(mult(a, b));


// --------------------------------------
// using interfaces
interface Person {
  firstName: string;
  lastName: string;
}

function greet(person: Person) {
  return `Hello, person.firstName person.lastName`;
}

let n: number = 20;
let user: Person = { firstName: "Jane", lastName: n };
console.log(greet(user));



// --------------------------------------
// CHALLENGES
// --------------------------------------
// implement a function to check if a number is even or not

// implement a Product and Cart interface. you should be able to
// add products to a cart.
