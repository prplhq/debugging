<?php

Namespace Model;

use ModelInterface\IVehicle;

abstract class Vehicle implements IVehicle {

  /**
  * @var Model\Garage[]
  */
  protected $garages;

  /**
   * Current speed in mph
   * @var int
   */
  public $currentSpeed = 0;

  /**
   * Get the list of garages
   * @return Model\Garage[]
   */
  public function getGarages() {
    return $this->garages;
  }

  /**
   * Add a garage to the list
   * @param Garage $garage
   * @return void
   */
  public function addGarage(Garage $garage) {
    $this->garages[] = $garage;
  }

  /**
   * Apply the brakes
   * @return void
   */
  public function brake() {
    echo "apply brake calipers\n";
    $this->currentSpeed = 0;
  }

  /**
   * Return current Speed
   * @return int
   */
  public function getSpeed() {
    echo "check the speedometer\n";
    return $this->currentSpeed;
  }


}
