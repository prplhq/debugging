<?php

Namespace Model;

use Model\Vehicle;
use ModelTrait\Engine;
use ModelInterface\IVehicle;

class Car extends Vehicle {
  // use Engine;

  /**
  * Top speed of the car in mph
  * @var int
  */
  public $topSpeed = 120;

  /**
  * Accelerate the car
  * @return void
  */
  public function accelerate($speed = 5) {
    echo "hit the gas\n";
    $this->currentSpeed += $speed;
  }

}
