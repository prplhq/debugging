<?php

Namespace Model;

use ModelInterface\IVehicle;

class Garage {

  /**
   * The name of the garage
   */
  // public $name;

  /**
   * Constructor method for garade
   * @param string $name
   */
  public function __construct($name) {
    $this->name = $name;
  }

  /**
   * Dictate how to display instances of this class
   * @return string
   */
  public function __toString() {
    return $this->name;
  }

  /**
   * Fix the vehicle that's been brought in
   * @param IVehicle $vehicle
   * @return void
   */
  public static function fixVehicle(IVehicle $vehicle) {
    echo "inspect\n";
    echo "order parts\n";
    echo "replace stuff\n";
  }

}
