<?php

Namespace Model;

use Model\Vehicle;
use ModelTrait\Engine;

class Truck extends Vehicle {
  use Engine;

  /**
   * Top speed of the truck in mph
   * @var int
   */
  public $topSpeed = 80;

  /**
   * Accelerate the truck
   * @return void
   */
  public function accelerate($speed = 5) {
    echo "hit the gas\n";
    $this->currentSpeed += $speed;
  }

  /**
   * Apply the brakes
   * @return void
   */
  public function brake() {
    echo "engage air tank to\n";
    echo "apply brake calipers\n";
    $this->currentSpeed = 0;
  }

}
