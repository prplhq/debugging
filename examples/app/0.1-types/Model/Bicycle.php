<?php

Namespace Model;

use Model\Vehicle;

class Bicycle extends Vehicle {

  /**
   * The top speed of the bike in mph
   * @var string
   */
  public $topSpeed = 30;

  /**
   * Accelerate the bike
   * @return void
   */
  public function accelerate($speed = 5) {
    echo "pedal faster\n";
    $this->currentSpeed += $speed;
  }

}