<?php

Namespace ModelTrait;

trait Trailer {

  /**
   * Attach the trailer
   * @return void
   */
  public function attachHitch() {
    echo "hook up the hitch\n";
    echo "plug in electrical\n";
  }

  /**
   * Unhook the trailer
   * @return void
   */
  public function detachHitch() {
    echo "unplug electrical\n";
    echo "detach the hitch\n";
  }

}