<?php

Namespace ModelTrait;

trait Engine {

  /**
   * Get the engine temp
   * @return void
   */
  public function getTemp() {
    echo "measure temp\n";
  }

  /**
   * Change the oil
   * @return void
   */
  public function changeOil() {
    echo "drain old oil\n";
    echo "add new oil\n";
  }
}
